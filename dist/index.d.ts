import App from "./App";
import AppPlayground from "./AppPlayground";
import { SurveyJSLibrary } from "./lib/SurveyJSLibrary";
import type { SurveyJSToolkitConfig } from "./utils/types/SurveyJSToolkitConfig";
interface InitializedApp {
    app: App;
    shadow: ShadowRoot;
    survey: typeof SurveyJSLibrary;
}
interface InitializedPlaygroundApp {
    app: AppPlayground;
    shadow: ShadowRoot;
    survey: typeof SurveyJSLibrary;
}
export declare const initialize: (data: SurveyJSToolkitConfig | unknown) => InitializedApp;
export declare const initializeModel: (data?: Record<string, unknown> | undefined) => SurveyJSLibrary.SurveyModel | null;
export declare const initializePlayground: (data: SurveyJSToolkitConfig | unknown) => InitializedPlaygroundApp;
export type { SurveyJSToolkitConfig };
export default initialize;
