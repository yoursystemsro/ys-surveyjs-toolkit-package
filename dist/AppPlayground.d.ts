import { LitElement, PropertyValues } from "lit";
import App from "./App";
import type { AppConfig } from "./utils/types/AppConfig";
export interface AppPlaygroundProps {
    config?: AppConfig;
}
declare type Properties = Record<keyof typeof AppPlayground.properties, unknown>;
export default class AppPlayground extends LitElement {
    static styles: import("lit").CSSResult;
    static properties: {
        config: {};
        error: {
            state: boolean;
        };
        loading: {
            state: boolean;
        };
        surveyData: {
            state: boolean;
        };
    };
    config: AppPlaygroundProps["config"];
    protected app: App;
    protected error: Error | null;
    protected loading: boolean;
    protected resultArea: HTMLTextAreaElement;
    protected surveyData: AppConfig["surveyData"] | null;
    constructor();
    protected updateSurveyData(e: Event): void;
    update(props: PropertyValues<Properties>): void;
    render(): import("lit-html").TemplateResult<1>;
    protected get renderLoading(): import("lit-html").TemplateResult<1> | null;
}
declare const componentName: string;
export { componentName };
