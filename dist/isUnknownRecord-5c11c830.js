function t(t){if(!t||"object"!=typeof t)return!1;const e={...t};return!Object.keys(e).some((t=>"string"!=typeof t))}export{t as i};
//# sourceMappingURL=isUnknownRecord-5c11c830.js.map
