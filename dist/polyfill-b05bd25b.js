const o="function"==typeof window.ShadowRoot.prototype.getSelection,t=window.navigator.userAgent.toLowerCase().indexOf("firefox")>-1;let e=!1;function n(){e||(e=!0,t&&(o||(window.ShadowRoot.prototype.getSelection=()=>document.getSelection())))}const i=()=>!t&&!o;export{i as c,n as p};
//# sourceMappingURL=polyfill-b05bd25b.js.map
