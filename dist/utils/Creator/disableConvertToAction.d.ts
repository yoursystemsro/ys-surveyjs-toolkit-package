import type { SurveyCreator } from "../../lib/SurveyJSCreator";
import { CustomCreatorWidget } from "../types/CustomWidget";
declare const disableConvertToAction: (creator: SurveyCreator, component: CustomCreatorWidget<unknown>) => void;
export default disableConvertToAction;
