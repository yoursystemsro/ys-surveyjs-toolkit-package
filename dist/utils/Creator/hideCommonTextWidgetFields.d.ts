import { CustomCreatorWidget } from "../types/CustomWidget";
declare const hideCommonTextWidgetFields: (component: CustomCreatorWidget<unknown, unknown>) => void;
export default hideCommonTextWidgetFields;
