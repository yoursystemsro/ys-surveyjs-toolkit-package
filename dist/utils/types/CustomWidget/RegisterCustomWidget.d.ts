import type { CustomWidget } from "./CustomWidget";
declare type WithoutOptions<T, RenderResult> = () => CustomWidget<T, RenderResult>;
declare type WithOptions<T, RenderResult, Options> = (options: Options) => CustomWidget<T, RenderResult>;
export declare type RegisterCustomWidget<T = Record<string, unknown>, RenderResult = unknown, Options = void> = Options extends void ? WithoutOptions<T, RenderResult> : WithOptions<T, RenderResult, Options>;
export {};
