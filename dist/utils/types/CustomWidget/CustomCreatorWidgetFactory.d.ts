import type { CustomCreatorWidget } from "./CustomCreatorWidget";
import type { CustomWidgetConfiguration } from "./CustomWidgetConfiguration";
export declare type CustomCreatorWidgetFactory<WidgetResult = unknown, RenderResult = unknown, ExtendedOptions = void> = (config: ExtendedOptions extends void ? CustomWidgetConfiguration : CustomWidgetConfiguration & ExtendedOptions) => CustomCreatorWidget<WidgetResult, RenderResult>;
