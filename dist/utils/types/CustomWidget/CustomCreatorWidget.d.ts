import type { CustomWidget } from "./CustomWidget";
import type { SurveyCreator } from "../../../lib/SurveyJSCreator";
export declare type CustomCreatorWidget<WidgetResult = unknown, RenderResult = unknown> = CustomWidget<WidgetResult, RenderResult> & {
    activatedByChanged?: (activatedBy: string) => unknown;
    afterInit?: (creator: SurveyCreator) => unknown;
    iconName?: string;
};
