import type { CustomCreatorWidget } from "./CustomCreatorWidget";
declare type WithoutOptions<WidgetResult, RenderResult> = () => CustomCreatorWidget<WidgetResult, RenderResult>;
declare type WithOptions<WidgetResult, RenderResult, Options> = (options: Options) => CustomCreatorWidget<WidgetResult, RenderResult>;
export declare type RegisterCustomCreatorWidget<WidgetResult = Record<string, unknown>, RenderResult = unknown, Options = void> = Options extends void ? WithoutOptions<WidgetResult, RenderResult> : WithOptions<WidgetResult, RenderResult, Options>;
export {};
