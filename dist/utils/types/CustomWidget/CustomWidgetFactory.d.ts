import type { CustomWidget } from "./CustomWidget";
import type { CustomWidgetConfiguration } from "./CustomWidgetConfiguration";
export declare type CustomWidgetFactory<WidgetResult = unknown, RenderResult = unknown, ExtendedOptions = void> = (config: ExtendedOptions extends void ? CustomWidgetConfiguration : CustomWidgetConfiguration & ExtendedOptions) => CustomWidget<WidgetResult, RenderResult>;
