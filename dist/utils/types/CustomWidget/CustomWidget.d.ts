import type { Question } from "../../../lib/SurveyJSCore";
export declare type CustomWidget<WidgetResult = unknown, RenderResult = unknown> = WidgetResult & {
    afterRender: (question: Question, el: HTMLElement) => RenderResult;
    category?: string;
    htmlTemplate?: string;
    init?: () => unknown;
    isDefaultRender?: boolean;
    isFit: (question: Question) => boolean;
    name: string;
    title: string;
    widgetIsLoaded: () => boolean;
    willUnmount?: (question: Question, el: HTMLElement) => unknown;
};
