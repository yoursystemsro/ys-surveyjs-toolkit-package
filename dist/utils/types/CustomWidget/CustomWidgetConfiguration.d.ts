import type { CreatorJsonMetadata } from "../../../lib/SurveyJSCreator";
import type { LibraryJsonMetadata } from "../../../lib/SurveyJSLibrary";
export interface CustomWidgetConfiguration {
    customName?: string;
    customTitle?: string;
    serializer: LibraryJsonMetadata | CreatorJsonMetadata;
}
