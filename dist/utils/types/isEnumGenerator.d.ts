/**
 * Generic function to generate enum typeguards
 * @param e
 */
declare const isEnumGenerator: <T>(e: T) => (token: unknown) => token is T[keyof T];
export default isEnumGenerator;
