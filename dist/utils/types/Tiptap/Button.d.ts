import type { Editor, Extensions } from "@tiptap/core";
export interface ButtonInterface {
    getExtensions: () => Extensions;
    init: (editor: Editor, target: Element | ShadowRoot, onActivate?: () => void) => void;
    updateButton: () => void;
}
export declare type ButtonFactory<T> = (options?: T) => ButtonInterface;
