import type { AppConfig } from "./AppConfig";
export interface SurveyJSToolkitConfig {
    appTarget: HTMLElement;
    config: AppConfig;
}
