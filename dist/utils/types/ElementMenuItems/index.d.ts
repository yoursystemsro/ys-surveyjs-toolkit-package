import type { Action } from "survey-core";
export interface DefineElementMenuItemsCallbackOptions {
    obj: {
        customWidgetValue?: {
            name?: string;
        };
    };
    items: Action[];
}
