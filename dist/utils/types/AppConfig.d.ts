import { SurveyJSOnCompleteOptions } from "../../components/SurveyJSForm/SurveyJSForm";
import { Question } from "../../lib/SurveyJSCore";
import { SurveyJSLibrary } from "../../lib/SurveyJSLibrary";
export declare enum PageType {
    COMPLETED = "completed",
    DEFAULT = "default",
    PREVIEW = "preview",
    START = "start"
}
export declare type PageData = {
    count: number;
    currentIndex: number;
    id: string;
    name: string;
    type: PageType.DEFAULT | PageType.START;
} | {
    type: PageType.PREVIEW;
} | {
    type: PageType.COMPLETED;
};
export interface AppConfig {
    acceptedFileTypes?: string[];
    locale?: string;
    title?: string;
    onAfterRenderQuestion?: (question: Question, htmlElement: HTMLElement) => unknown;
    onComplete?: (result: Record<string, unknown>, options: SurveyJSOnCompleteOptions) => unknown;
    onCreate?: (survey: SurveyJSLibrary.Survey) => unknown;
    onRendered?: () => unknown;
    onPage?: (d: PageData) => unknown;
    responsiveImageSizesAttribute?: string;
    surveyData: Record<string, unknown>;
    themes?: (string | HTMLStyleElement)[];
    voiceSupport?: boolean;
}
