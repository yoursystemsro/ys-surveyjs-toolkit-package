import { QuestionFileModel } from "survey-core";
declare const _default: (widget: unknown) => widget is QuestionFileModel;
export default _default;
