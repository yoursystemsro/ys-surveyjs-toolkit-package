export default function isUnknownRecord(data: unknown): data is Record<string, unknown>;
