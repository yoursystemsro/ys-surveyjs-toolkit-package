import type { AppConfig } from "./types/AppConfig";
export default function isAppConfig(data: unknown, setError: (error: Error) => void): data is AppConfig;
