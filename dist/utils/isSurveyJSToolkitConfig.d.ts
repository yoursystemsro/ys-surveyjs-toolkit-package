import type { SurveyJSToolkitConfig } from "./types/SurveyJSToolkitConfig";
export default function isSurveyJSToolkitConfig(data: unknown, setError: (error: Error) => void): data is SurveyJSToolkitConfig;
