interface ReturnType {
    getBindingElement: () => HTMLElement;
    parentElement: HTMLElement;
}
declare const makeContentEditableCustomQuestion: (el: HTMLElement) => ReturnType;
export default makeContentEditableCustomQuestion;
