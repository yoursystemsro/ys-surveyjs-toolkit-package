declare const registerCustomElement: (namespace: string, element: CustomElementConstructor, className: string) => string;
export default registerCustomElement;
