declare type PartialPageModel = {
    propertyHash: {
        id: () => string;
    };
};
declare const isPartialPage: (data: unknown) => data is PartialPageModel;
export default isPartialPage;
