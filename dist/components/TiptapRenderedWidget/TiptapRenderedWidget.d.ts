/// <reference types="react" />
import { Editor } from "@tiptap/core";
import { Question } from "survey-core";
import { CustomWidget } from "../../utils/types/CustomWidget";
import { CreateTiptapEditorProps } from "../PropertyWysiwygEditor";
import { HTMLContentWidget } from "../../lib/HtmlContentWidget";
export interface TiptapRenderedWidgetProps extends Omit<CreateTiptapEditorProps, "setPropertyValue"> {
    component: CustomWidget<HTMLContentWidget>;
    question: Question;
    onRender?: (editor: Editor) => unknown;
}
declare const TiptapRenderedWidget: (props: TiptapRenderedWidgetProps) => JSX.Element;
export default TiptapRenderedWidget;
