import { LitElement, PropertyValues } from "lit";
import DeviceSelector, { OrderedMediaDevice } from "./DeviceSelector";
import Scanner from "./Scanner";
import { WidgetLocalization } from "./Widget";
interface ConstructorOptions {
    active?: boolean;
    localization: WidgetLocalization;
    onSuccess: (value: string) => unknown;
    onClose: () => unknown;
}
declare type DialogProperties = {
    active: boolean;
    currentDevice?: OrderedMediaDevice;
    localization: WidgetLocalization;
    scanner?: Scanner;
    selector?: DeviceSelector;
};
export default class Dialog extends LitElement {
    static properties: {
        active: {
            type: BooleanConstructor;
        };
        currentDevice: {};
        hasMultipleDevices: {};
        localization: {};
        onClose: {};
        onSuccess: {};
        scanner: {};
        selector: {};
    };
    active: DialogProperties["active"];
    localization: ConstructorOptions["localization"];
    devices: OrderedMediaDevice[];
    onClose: ConstructorOptions["onClose"];
    onSuccess: ConstructorOptions["onSuccess"];
    protected currentDevice: DialogProperties["currentDevice"];
    protected scanner: DialogProperties["scanner"];
    protected selector: DialogProperties["selector"];
    constructor(options: ConstructorOptions);
    protected close(): void;
    protected openSelector(manual?: boolean): void;
    protected openScanner(): void;
    protected update(props: PropertyValues<DialogProperties>): void;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
    disconnectedCallback(): void;
}
export {};
