import { LitElement, PropertyValues } from "lit";
import Dialog from "./Dialog";
export declare type WidgetLocalization = {
    accessError: string;
    changeDeviceButtonLabel: string;
    inputPlaceholder: string;
    loadingText: string;
    noDeviceError: string;
    selectCamera: string;
    scanButtonLabel: string;
    scannerError: string;
};
interface ConstructorOptions {
    disabled: boolean;
    localization: WidgetLocalization;
    onChange: (value: string) => unknown;
    value: string;
}
declare type WidgetProperties = {
    active: boolean;
    localization: WidgetLocalization;
    value: string;
};
export default class Widget extends LitElement {
    static properties: {
        active: {
            state: boolean;
            type: BooleanConstructor;
        };
        disabled: {
            state: boolean;
            type: BooleanConstructor;
        };
        localization: {
            state: boolean;
        };
        value: {
            state: boolean;
            type: StringConstructor;
        };
        onChange: {
            state: boolean;
        };
    };
    disabled: boolean;
    onChange: (value: string) => void;
    localization: WidgetLocalization;
    value: string;
    protected active: boolean;
    protected dialog: Dialog;
    protected changeHandler: (e: Event) => void;
    constructor(options: ConstructorOptions);
    protected updated(props: PropertyValues<WidgetProperties>): void;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
    protected openScannerDialog(): void;
}
export {};
