import { LitElement } from "lit";
import { WidgetLocalization } from "./Widget";
export declare type OrderedMediaDevice = {
    device: MediaDeviceInfo;
    order: number;
};
interface ConstructorOptions {
    localization: WidgetLocalization;
    manual?: boolean;
    onDeviceAvailability?: (devices: OrderedMediaDevice[]) => unknown;
    onSelect: (device: OrderedMediaDevice) => unknown;
}
export default class DeviceSelector extends LitElement {
    static properties: {
        errors: {
            state: boolean;
            type: ArrayConstructor;
        };
        localization: {};
        manual: {};
        onDeviceAvailability: {};
        onSelect: {};
        requestingDevice: {};
    };
    errors: string[];
    localization: ConstructorOptions["localization"];
    manual: ConstructorOptions["manual"];
    onDeviceAvailability: ConstructorOptions["onDeviceAvailability"];
    onSelect: ConstructorOptions["onSelect"];
    protected changeHandler: (e: Event) => void;
    protected requestingDevice: boolean;
    protected set rememberedDevice(d: OrderedMediaDevice | null);
    protected get rememberedDevice(): OrderedMediaDevice | null;
    protected videoDevices: OrderedMediaDevice[];
    constructor(options: ConstructorOptions);
    static changeRememberedDevice(d: OrderedMediaDevice | null, videoDevices: OrderedMediaDevice[]): void;
    static removeRememberedDevice(): void;
    protected enumerateDevices(): Promise<void>;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
}
export {};
