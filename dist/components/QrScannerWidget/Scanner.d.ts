import { LitElement, TemplateResult } from "lit";
import type { Symbol as ZbarSymbol } from "zbar.wasm";
import { WidgetLocalization } from "./Widget";
interface ConstructorOptions {
    deviceId: string;
    localization: WidgetLocalization;
    onChangeDevice?: () => unknown;
    onScan: (value: string) => unknown;
}
export default class Scanner extends LitElement {
    static properties: {
        error: {};
        loading: {};
        localization: {};
        onChangeDevice: {};
        onScan: {};
        ratio: {};
        stream: {};
        video: {};
    };
    localization: ConstructorOptions["localization"];
    onChangeDevice?: () => unknown;
    onScan: (value: string) => unknown;
    protected canvas: HTMLCanvasElement;
    protected deviceId: string;
    protected errors: string[];
    protected loading: boolean;
    protected ratio?: number;
    protected stream?: MediaStream;
    protected video?: HTMLVideoElement;
    protected scanningActive: boolean;
    protected scanFn?: (d: ImageData) => Promise<ZbarSymbol[]>;
    constructor(options: ConstructorOptions);
    protected loadScanFn(): Promise<(d: ImageData) => Promise<ZbarSymbol[]>>;
    protected scanImageData(d: ImageData): Promise<ZbarSymbol[]>;
    protected scanImage(): Promise<string | null>;
    protected startVideo(): Promise<void>;
    protected stopVideo(): void;
    render(): TemplateResult<1>;
    createRenderRoot(): this;
    disconnectedCallback(): void;
}
export {};
