import { LitElement, PropertyValues } from "lit";
export declare type WidgetLocalization = {
    approachingLimitLabel: string;
    connectingLabel: string;
    listeningLabel: string;
    microphoneNotAllowedLabel: string;
    requestingMicrophoneLabel: string;
    speechToTextAvailableLabel: string;
    startListeningLabel: string;
    stopListeningLabel: string;
    stoppingConnectionLabel: string;
};
interface ConstructorOptions {
    disabled: boolean;
    enableSpeechToText: boolean;
    localization: WidgetLocalization;
    onChange: (value: string) => unknown;
    speechToTextTimeLimit?: number;
    value: string;
}
declare type WidgetProperties = {
    localization: WidgetLocalization;
    onStartRecording: (stream: MediaStream) => Promise<void>;
    onStopRecording: () => Promise<void>;
    sttErrors: string[];
    value: string;
};
export default class Widget extends LitElement {
    static properties: {
        approachingLimit: {
            state: boolean;
            type: BooleanConstructor;
        };
        disabled: {
            state: boolean;
            type: BooleanConstructor;
        };
        enableSpeechToText: {
            state: boolean;
            type: BooleanConstructor;
        };
        onStartRecording: {
            state: boolean;
        };
        onStopRecording: {
            state: boolean;
        };
        initializingStt: {
            state: boolean;
            type: BooleanConstructor;
        };
        localization: {
            state: boolean;
        };
        onChange: {
            state: boolean;
        };
        recording: {
            state: boolean;
            type: BooleanConstructor;
        };
        speechToTextTimeLimit: {
            state: boolean;
            type: NumberConstructor;
        };
        stoppingRecording: {
            state: boolean;
            type: BooleanConstructor;
        };
        sttAvailable: {
            state: boolean;
            type: BooleanConstructor;
        };
        sttErrors: {
            state: boolean;
            type: ArrayConstructor;
        };
        timeLeft: {
            state: boolean;
            type: NumberConstructor;
        };
        value: {
            state: boolean;
            type: StringConstructor;
        };
        waitingForConnection: {
            state: boolean;
            type: BooleanConstructor;
        };
    };
    disabled: boolean;
    enableSpeechToText: boolean;
    localization: WidgetLocalization;
    onChange: (value: string) => void;
    onStartRecording: null | ((steam: MediaStream) => Promise<void>);
    onStopRecording: null | (() => Promise<void>);
    sttErrors: string[];
    value: string;
    protected initializingStt: boolean;
    protected changeHandler: (e: Event) => void;
    protected recording: boolean;
    /**
     * Timeout that triggers the user warning that the recording will stop momentarily
     */
    protected recordingLimitCountdown: number | null;
    /**
     * Interval that runs once the countdown has expired, then stops recording
     */
    protected recordingStopInterval: number | null;
    protected speechToTextTimeLimit: number;
    protected stoppingRecording: boolean;
    protected stream: null | MediaStream;
    protected sttAvailable: boolean;
    /**
     * Number of milliseconds until the recording is automatically stopped
     */
    protected timeLeft: number;
    protected waitingForConnection: boolean;
    protected get isDisabled(): boolean;
    protected get textAreaDisabled(): boolean;
    constructor(options: ConstructorOptions);
    stopRecording(): Promise<void>;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
    protected updated(props: PropertyValues<WidgetProperties>): void;
    protected buttonClickHandler(ev: MouseEvent): void;
    protected checkSttRequirements(): Promise<void>;
    protected renderInfo(): import("lit-html").TemplateResult<1>;
    protected renderStt(): import("lit-html").TemplateResult<1> | null;
    protected startRecording(): Promise<void>;
}
export {};
