import { LitElement, TemplateResult } from "lit";
import type { ClassInfo } from "lit/directives/class-map.js";
export declare enum AssetType {
    IMAGE = "IMAGE",
    VIDEO = "VIDEO",
    OTHER = "OTHER"
}
interface BaseWidgetProps {
    assetType: AssetType;
    name: string;
    url: string;
}
interface ImageFormat {
    url: string;
    width: number;
}
interface ImageWidgetProps extends BaseWidgetProps {
    assetType: AssetType.IMAGE;
    formats?: ImageFormat[];
    sizes?: string;
}
interface VideoWidgetProps extends BaseWidgetProps {
    assetType: AssetType.VIDEO;
    sources: {
        [mimeType: string]: {
            src: string;
        };
    };
}
interface OtherWidgetProps extends BaseWidgetProps {
    assetType: AssetType.OTHER;
}
export declare type WidgetProps = ImageWidgetProps | OtherWidgetProps | VideoWidgetProps;
export declare const isImageProps: (props: BaseWidgetProps) => props is ImageWidgetProps;
export default class Widget extends LitElement {
    static properties: {
        caption: {};
        config: {};
    };
    caption?: string;
    config: WidgetProps;
    protected classInfo: ClassInfo;
    constructor();
    protected get figcaption(): TemplateResult | null;
    render(): TemplateResult<1>;
    createRenderRoot(): this;
}
export {};
