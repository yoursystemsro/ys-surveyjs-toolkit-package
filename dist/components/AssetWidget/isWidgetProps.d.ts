import { WidgetProps } from "./Widget";
declare const isWidgetProps: (data: unknown) => data is WidgetProps;
export default isWidgetProps;
