import { LitElement } from "lit";
interface Source {
    url: string;
    width: number;
}
export default class Image extends LitElement {
    static properties: {
        alt: {};
        sizes: {};
        sources: {};
        src: {};
    };
    alt: string;
    sizes?: string;
    sources: Source[];
    src: string;
    constructor();
    get srcset(): string;
    renderImage(): import("lit-html").TemplateResult<1>;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
}
export {};
