import { LitElement } from "lit";
interface Sources {
    [mimeType: string]: {
        src: string;
    };
}
export default class Video extends LitElement {
    static properties: {
        name: {};
        sources: {};
        src: {};
    };
    name: string;
    sources: Sources;
    src?: string;
    constructor();
    renderSources(): import("lit-html").TemplateResult<1>[];
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
}
export {};
