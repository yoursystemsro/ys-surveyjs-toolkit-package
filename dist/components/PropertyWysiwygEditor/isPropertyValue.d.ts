import { EditorValue } from "./useTiptapEditor";
declare const isPropertyValue: (value: unknown) => value is EditorValue;
export default isPropertyValue;
