import { Editor, EditorOptions } from "@tiptap/core";
import type { ButtonInterface } from "../../utils/types/Tiptap";
export interface EditorValue {
    html?: string;
    json?: Record<string, unknown>;
}
export interface Props {
    buttons?: ButtonInterface[];
    extensions: EditorOptions["extensions"];
    initialValue?: EditorValue;
    setPropertyValue: (value: EditorValue) => void;
    themes?: HTMLStyleElement[];
    useShadowRoot?: boolean;
}
interface Result {
    render: (target: HTMLElement) => Editor | null;
    setEditorValue: (value: EditorValue) => void;
}
declare type Hook = (props: Props) => Result;
declare const useTiptapEditor: Hook;
export default useTiptapEditor;
