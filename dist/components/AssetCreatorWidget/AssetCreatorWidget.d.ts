import React from "react";
import { AssetWidgetProps } from "../AssetWidget";
export interface AssetCreatorWidgetProps {
    config?: AssetWidgetProps;
    label: {
        whenEmpty: string;
        whenSelected: string;
    };
    onSelect: () => void;
}
declare const AssetCreatorWidget: React.FC<AssetCreatorWidgetProps>;
export default AssetCreatorWidget;
