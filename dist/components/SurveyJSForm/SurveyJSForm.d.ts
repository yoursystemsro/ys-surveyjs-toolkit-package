import { LitElement, PropertyValues } from "lit";
import { SurveyJSLibrary } from "../../lib/SurveyJSLibrary";
import { AppConfig } from "../../utils/types/AppConfig";
export interface SurveyJSFormProps {
    acceptedFileTypes: AppConfig["acceptedFileTypes"];
    imageSizesAttribute: string;
    locale: AppConfig["locale"];
    onAfterRenderQuestion: AppConfig["onAfterRenderQuestion"];
    onComplete: AppConfig["onComplete"];
    onCreate: AppConfig["onCreate"];
    onRendered: AppConfig["onRendered"];
    onPage: AppConfig["onPage"];
    surveyData: AppConfig["surveyData"];
}
export interface SurveyJSOnCompleteOptions {
    showDataSaving: (text?: string) => void;
    showDataSavingError: (text?: string) => void;
    showDataSavingSuccess: (text?: string) => void;
    showDataSavingClear: () => void;
    isCompleteOnTrigger: boolean;
}
declare type Properties = Record<keyof typeof SurveyJSForm.properties, unknown>;
export default class SurveyJSForm extends LitElement {
    static properties: {
        acceptedFileTypes: {};
        imageSizesAttribute: {};
        locale: {};
        onComplete: {};
        onCreate: {};
        onRendered: {};
        surveyData: {};
        survey: {
            state: boolean;
        };
    };
    acceptedFileTypes?: SurveyJSFormProps["acceptedFileTypes"];
    imageSizesAttribute?: SurveyJSFormProps["imageSizesAttribute"];
    locale?: SurveyJSFormProps["locale"];
    onAfterRenderQuestion?: SurveyJSFormProps["onAfterRenderQuestion"];
    onComplete?: SurveyJSFormProps["onComplete"];
    onCreate?: SurveyJSFormProps["onCreate"];
    onRendered: SurveyJSFormProps["onRendered"];
    onPage: SurveyJSFormProps["onPage"];
    surveyData?: SurveyJSFormProps["surveyData"];
    protected survey?: SurveyJSLibrary.Survey;
    protected wrapper: HTMLDivElement;
    constructor();
    update(changedProperties: PropertyValues<Properties>): void;
    protected initializeSurvey(): void;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): this;
}
declare const componentName: string;
export { componentName };
