export { default as SurveyJSForm, componentName as SurveyJSFormComponentName, } from "./SurveyJSForm";
export type { SurveyJSFormProps } from "./SurveyJSForm";
