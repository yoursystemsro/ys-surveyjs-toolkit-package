import React from "react";
declare type Props = {
    enableSpeechToText: boolean;
};
declare const TextInputWithSTTCreatorWidget: React.FC<Props>;
export default TextInputWithSTTCreatorWidget;
