import{L as e,a as t}from"./index-132ed4af.js";import{C as s}from"./HtmlContentWidgetTypes-52484f44.js";const a=a=>{const{name:m}=a,o=s({customName:m,serializer:t});return e.getCustomWidgetByName(o.name)||e.add(o,"customtype"),o};export{a as R};
//# sourceMappingURL=RegisterWidget-35dd02bb.js.map
