import{T as s,r as e,s as t,p as r}from"./registerCustomElement-49181dcc.js";class i extends t{constructor(){super(),this.alt="",this.sources=[],this.src=""}get srcset(){return this.sources.map((s=>`${s.url} ${s.width}w`)).join(",")}renderImage(){return""!==this.srcset.trim()?r`<img
        src=${this.src}
        srcset=${this.srcset}
        sizes=${(e=>null!=e?e:s)(this.sizes)}
        alt=${this.alt}
      />`:r`<img src=${this.src} alt=${this.alt} />`}render(){return r`<div>
      <style scoped>
        img {
          max-width: 100%;
        }
      </style>
      ${this.renderImage()}
    </div>`}createRenderRoot(){return this}}i.properties={alt:{},sizes:{},sources:{},src:{}},e("library-asset-widget",i,"Image");class c extends t{constructor(){super(),this.name="",this.sources={}}renderSources(){return Object.entries(this.sources).map((([s,e])=>r`<source src=${e.src} type=${s} />`))}render(){return r`<div>
      <style scoped>
        video {
          max-width: 100%;
        }
      </style>
      <video controls title=${this.name} ?src=${this.src}>
        ${this.renderSources()}
      </video>
    </div>`}createRenderRoot(){return this}}var n;c.properties={name:{},sources:{},src:{}},e("library-asset-widget",c,"Video"),function(s){s.IMAGE="IMAGE",s.VIDEO="VIDEO",s.OTHER="OTHER"}(n||(n={}));const o=s=>s.assetType===n.IMAGE;class a extends t{constructor(){super(),this.classInfo={control:!0,"is-active":!1},this.config={assetType:n.OTHER,name:"",url:""}}get figcaption(){return this.caption?r`<figcaption>${this.caption}</figcaption>`:null}render(){let s;return o(this.config)?(s=new i,s.src=this.config.url,s.alt=this.config.name,this.config.sizes&&(s.sizes=this.config.sizes),this.config.formats&&(s.sources=this.config.formats)):this.config.assetType===n.VIDEO?(s=new c,s.name=this.config.name,s.sources=this.config.sources,Object.keys(this.config.sources).length<1&&(s.src=this.config.url)):s=r`<a href=${this.config.url} target="_blank"
        >${this.config.name}</a
      >`,r` <figure>${s} ${this.figcaption}</figure> `}createRenderRoot(){return this}}a.properties={caption:{},config:{}},e("library-asset-widget",a,"Widget");export{n as A,a as W,o as i};
//# sourceMappingURL=Widget-cafd40b8.js.map
