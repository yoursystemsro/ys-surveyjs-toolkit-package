import{a as t,L as e}from"./index-132ed4af.js";import{r as i,s,p as n}from"./registerCustomElement-49181dcc.js";import{o}from"./class-map-77cc9860.js";import{c as a,l as r,e as l,s as c}from"./createProperties-d2fd7a38.js";const h="yst-text-input-stt",d=t=>console.error("Text input with SpeechToText widget error:",t);class g extends s{constructor(t){super(),this.disabled=t.disabled,this.enableSpeechToText=t.enableSpeechToText,this.localization=t.localization,this.onChange=t.onChange,this.value=t.value,this.sttErrors=[],this.initializingStt=!1,this.onStartRecording=null,this.onStopRecording=null,this.recording=!1,this.recordingLimitCountdown=null,this.recordingStopInterval=null,this.timeLeft=-1,this.stoppingRecording=!1,this.stream=null,this.sttAvailable=!1,this.waitingForConnection=!1,this.speechToTextTimeLimit=t.speechToTextTimeLimit&&t.speechToTextTimeLimit>0?t.speechToTextTimeLimit:12e4,this.changeHandler=t=>{t.target instanceof HTMLTextAreaElement&&(this.value=t.target.value,this.onChange(t.target.value))}}get isDisabled(){return this.disabled||this.initializingStt||this.waitingForConnection||this.stoppingRecording}get textAreaDisabled(){return this.disabled||this.initializingStt||this.waitingForConnection||this.recording}async stopRecording(){this.stoppingRecording=!0,this.recordingLimitCountdown&&window.clearTimeout(this.recordingLimitCountdown),this.recordingStopInterval&&window.clearInterval(this.recordingStopInterval),this.onStopRecording&&await this.onStopRecording(),this.stream&&this.stream.getAudioTracks().forEach((t=>t.stop())),this.timeLeft=-1,this.waitingForConnection=!1,this.initializingStt=!1,this.recording=!1,this.stoppingRecording=!1}render(){const t={[h]:!0,"is-disabled":this.disabled};return n`
      <div class=${o(t)}>
        ${this.renderStt()}
        <label>
          <textarea
            class=${`${h}__input`}
            ?disabled=${this.textAreaDisabled}
            @input=${this.changeHandler}
            @change=${this.changeHandler}
            .value=${this.value}
          ></textarea>
        </label>
      </div>
    `}createRenderRoot(){return this}updated(t){super.update(t),t.has("value")&&this.onChange(this.value),t.has("onStartRecording")&&(this.onStartRecording?this.checkSttRequirements().catch(d):this.sttAvailable=!1),t.has("sttErrors")&&this.stopRecording().catch(d)}buttonClickHandler(t){t.preventDefault(),this.recording?this.stopRecording().catch(d):this.startRecording().catch(d)}async checkSttRequirements(){const t=(await window.navigator.mediaDevices.enumerateDevices()).some((t=>"audioinput"===t.kind));this.sttAvailable=t&&"function"==typeof this.onStartRecording}renderInfo(){if(this.sttErrors.length>0)return n`
        ${this.sttErrors.map((t=>n`<p class=${`${h}__stt-info__error`}>${t}</p>`))}
      `;if(this.initializingStt)return n`<p class=${`${h}__stt-info__message`}>
        ${this.localization.requestingMicrophoneLabel}
      </p>`;if(this.waitingForConnection)return n`<p class=${`${h}__stt-info__message`}>
        ${this.localization.connectingLabel}
      </p>`;if(this.stoppingRecording)return n`<p class=${`${h}__stt-info__message`}>
        ${this.localization.stoppingConnectionLabel}
      </p>`;if(this.recording){if(this.timeLeft>=0){let t=`${Math.floor(this.timeLeft/1e3/60)}`,e=`${Math.floor(this.timeLeft/1e3%60)}`;return t.length<2&&(t=`0${t}`),e.length<2&&(e=`0${e}`),n`<p
          class=${`${h}__stt-info__message ${h}__stt-info__message--countdown`}
        >
          ${this.localization.approachingLimitLabel.replaceAll("{time}",`${t}:${e}`)}
        </p>`}return n`<p class=${`${h}__stt-info__message`}>
        ${this.localization.listeningLabel}
      </p>`}return n`<p class=${`${h}__stt-info__message`}>
      ${this.localization.speechToTextAvailableLabel}
    </p>`}renderStt(){return this.enableSpeechToText&&this.sttAvailable?n`
      <div class=${`${h}__stt-controls`}>
        <div class=${`${h}__stt-info`}>${this.renderInfo()}</div>
        <button
          class=${`${h}__stt-controls__button`}
          ?disabled=${this.isDisabled}
          @click=${this.buttonClickHandler}
        >
          ${this.recording?this.localization.stopListeningLabel:this.localization.startListeningLabel}
        </button>
      </div>
    `:null}async startRecording(){this.sttErrors=[],this.initializingStt=!0,this.stoppingRecording=!1,this.recordingLimitCountdown&&window.clearTimeout(this.recordingLimitCountdown),this.recordingStopInterval&&window.clearInterval(this.recordingStopInterval),this.stream&&this.stream.getAudioTracks().forEach((t=>t.stop()));try{this.stream=await window.navigator.mediaDevices.getUserMedia({audio:!0,video:!1})}catch(t){throw this.sttErrors=[this.localization.microphoneNotAllowedLabel],t}finally{this.initializingStt=!1}try{this.waitingForConnection=!0,this.onStartRecording&&await this.onStartRecording(this.stream),this.recording=!0,this.waitingForConnection=!1;const t=3e4,e=this.speechToTextTimeLimit>t?t:this.speechToTextTimeLimit,i=e===this.speechToTextTimeLimit?100:this.speechToTextTimeLimit-e;this.recordingLimitCountdown=window.setTimeout((()=>{this.timeLeft=e,this.recordingStopInterval=window.setInterval((()=>{this.timeLeft-=250,this.timeLeft<=0&&(this.recordingStopInterval&&window.clearInterval(this.recordingStopInterval),this.stopRecording().catch(d))}),250)}),i)}catch(t){throw this.recording=!1,t}finally{this.initializingStt=!1,this.waitingForConnection=!1}}}g.properties={approachingLimit:{state:!0,type:Boolean},disabled:{state:!0,type:Boolean},enableSpeechToText:{state:!0,type:Boolean},onStartRecording:{state:!0},onStopRecording:{state:!0},initializingStt:{state:!0,type:Boolean},localization:{state:!0},onChange:{state:!0},recording:{state:!0,type:Boolean},speechToTextTimeLimit:{state:!0,type:Number},stoppingRecording:{state:!0,type:Boolean},sttAvailable:{state:!0,type:Boolean},sttErrors:{state:!0,type:Array},timeLeft:{state:!0,type:Number},value:{state:!0,type:String},waitingForConnection:{state:!0,type:Boolean}},i("text-input-stt-widget",g,"Widget");const p="yst-widget--text-input-stt",u=t=>{const{customName:e,customTitle:i,serializer:s,voiceSupport:n}=t,o=e||"text-input-stt-widget";return{name:o,title:i||"Text input",widgetIsLoaded:()=>!0,init:()=>{s.addClass(o,[],void 0,"empty"),a(o,s,n)},isDefaultRender:!0,isFit:t=>t.getType()===o,afterRender:(t,e)=>{let i="";"string"==typeof t.value&&(i=t.value);const s={approachingLimitLabel:"",connectingLabel:"",listeningLabel:"",microphoneNotAllowedLabel:"",requestingMicrophoneLabel:"",speechToTextAvailableLabel:"",startListeningLabel:"",stopListeningLabel:"",stoppingConnectionLabel:""};r.forEach((e=>{let i=t.getLocalizableStringText(e.name);if(""===i.trim()){const s=t.getPropertyByName(e.name);i="string"==typeof s.defaultValue?s.defaultValue:""}s[e.name]=i}));const o=n&&t.getPropertyByName(l).getPropertyValue(t),a=n&&t.getPropertyByName(c).getPropertyValue(t),h=new g({disabled:t.isReadOnly,enableSpeechToText:"boolean"==typeof o&&o,localization:s,onChange:e=>{t.value=e},speechToTextTimeLimit:"number"==typeof a?a:void 0,value:i});return t.valueChangedCallback=()=>{h.value="string"==typeof t.value?t.value:""},h.setAttribute("class","yst-widget yst-widget--text-input-stt"),e.appendChild(h),h}}},m=i=>{const s=u({serializer:t,voiceSupport:i.voiceSupport||!1});return e.getCustomWidgetByName(s.name)||e.add(s,"customtype"),s};export{u as C,m as R,g as W,p as c};
//# sourceMappingURL=RegisterWidget-98c7a93d.js.map
