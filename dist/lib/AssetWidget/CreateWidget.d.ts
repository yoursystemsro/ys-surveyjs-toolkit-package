import type { CustomWidgetFactory } from "../../utils/types/CustomWidget";
import { AssetWidget } from "../../components/AssetWidget";
export interface Widget {
    assetPropertyType: string;
    assetPropertyName: string;
}
declare const CreateWidget: CustomWidgetFactory<Widget, AssetWidget>;
export default CreateWidget;
