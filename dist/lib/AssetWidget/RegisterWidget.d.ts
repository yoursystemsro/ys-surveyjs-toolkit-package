import type { RegisterCustomWidget } from "../../utils/types/CustomWidget";
import type { Widget } from "./CreateWidget";
declare const RegisterWidget: RegisterCustomWidget<Widget>;
export default RegisterWidget;
