import { TiptapButtonProps } from "../../internal/TiptapButton";
import type { ButtonFactory } from "../../utils/types/Tiptap";
interface Options {
    props: Partial<TiptapButtonProps>;
}
declare const createButton: ButtonFactory<Partial<Options>>;
export default createButton;
