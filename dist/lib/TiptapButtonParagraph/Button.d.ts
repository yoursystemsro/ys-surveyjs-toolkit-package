import type { Editor } from "@tiptap/core";
import { TiptapButton } from "../../internal/TiptapButton";
export declare enum ParagraphButtonEvents {
    ACTIVATE = "ACTIVATE"
}
export default class ParagraphButton extends TiptapButton {
    constructor(editor: Editor);
    protected isActive(): boolean;
    protected onClick(e: MouseEvent): void;
}
