import type { CustomCreatorWidgetFactory } from "../../utils/types/CustomWidget";
declare const CreateWidget: CustomCreatorWidgetFactory<unknown, unknown, {
    voiceSupport: boolean;
}>;
export default CreateWidget;
