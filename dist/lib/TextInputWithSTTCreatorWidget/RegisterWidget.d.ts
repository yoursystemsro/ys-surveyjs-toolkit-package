import type { RegisterCustomCreatorWidget } from "../../utils/types/CustomWidget";
declare const RegisterWidget: RegisterCustomCreatorWidget<unknown, unknown, {
    voiceSupport?: boolean;
}>;
export default RegisterWidget;
