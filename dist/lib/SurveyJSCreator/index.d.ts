import { SurveyCreator, SurveyCreatorComponent } from "survey-creator-react";
import * as CreatorModule from "survey-creator-react";
import { ICreatorOptions } from "survey-creator";
import { CustomWidgetCollection } from "survey-core";
export type { CustomWidgetCollection as CustomCreatoridgetCollection, JsonMetadata as CreatorJsonMetadata, } from "survey-core";
export declare const CreatorSerializer: import("survey-core").JsonMetadata;
export declare const CreatorWidgetCollection: CustomWidgetCollection;
export { CreatorModule, SurveyCreator, SurveyCreatorComponent };
export type { ICreatorOptions };
