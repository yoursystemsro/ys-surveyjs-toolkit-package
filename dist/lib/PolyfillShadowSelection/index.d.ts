import polyfill, { cannotBePolyfilled } from "./polyfill";
export default polyfill;
export { cannotBePolyfilled };
