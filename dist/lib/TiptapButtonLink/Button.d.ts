import type { Editor } from "@tiptap/core";
import { TiptapButton } from "../../internal/TiptapButton";
import { ButtonProps } from "./createButton";
export default class LinkButton extends TiptapButton {
    static properties: {
        preventDisable: {};
        label: {};
    };
    preventDisable: ButtonProps["preventDisable"];
    constructor(editor: Editor);
    protected isActive(): boolean;
    protected onClick(e: MouseEvent): void;
}
