import type { Editor } from "@tiptap/core";
import { TiptapButton, TiptapButtonProps } from "../../internal/TiptapButton";
export interface HeadingButtonProps extends TiptapButtonProps {
    level: 1 | 2 | 3 | 4 | 5 | 6;
    enableToggle: boolean;
}
export declare enum HeadingButtonEvents {
    ACTIVATE = "ACTIVATE"
}
export default class HeadingButton extends TiptapButton {
    static properties: {
        level: {};
        label: {};
    };
    level: HeadingButtonProps["level"];
    enableToggle: HeadingButtonProps["enableToggle"];
    constructor(editor: Editor);
    protected isActive(): boolean;
    protected onClick(e: MouseEvent): void;
}
