import type { ButtonFactory } from "../../utils/types/Tiptap";
import type { HeadingButtonProps } from "./Button";
interface Options {
    props: Partial<HeadingButtonProps>;
}
declare const createButton: ButtonFactory<Partial<Options>>;
export default createButton;
