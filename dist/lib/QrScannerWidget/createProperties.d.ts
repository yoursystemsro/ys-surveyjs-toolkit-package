import { WidgetLocalization } from "../../components/QrScannerWidget";
import type { CreatorJsonMetadata } from "../SurveyJSCreator";
import type { LibraryJsonMetadata } from "../SurveyJSLibrary";
export declare type Property = keyof WidgetLocalization;
export declare const properties: readonly Property[];
declare const createProperties: (name: string, serializer: CreatorJsonMetadata | LibraryJsonMetadata) => void;
export default createProperties;
