import type { CustomWidgetFactory } from "../../utils/types/CustomWidget";
import { QrScannerWidget } from "../../components/QrScannerWidget";
declare const CreateWidget: CustomWidgetFactory<unknown, QrScannerWidget>;
export default CreateWidget;
