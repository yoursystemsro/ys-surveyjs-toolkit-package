import * as SurveyJSLibrary from "survey-knockout";
export type { CustomWidgetCollection as CustomLibraryWidgetCollection, JsonMetadata as LibraryJsonMetadata, } from "survey-knockout";
export declare const LibrarySerializer: SurveyJSLibrary.JsonMetadata;
export declare const LibraryWidgetCollection: SurveyJSLibrary.CustomWidgetCollection;
export { SurveyJSLibrary };
