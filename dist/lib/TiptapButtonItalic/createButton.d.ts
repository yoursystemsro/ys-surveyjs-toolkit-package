import type { ButtonFactory } from "../../utils/types/Tiptap";
export interface ButtonProps {
    label?: string;
}
interface Options {
    props?: ButtonProps;
}
declare const createButton: ButtonFactory<Options>;
export default createButton;
