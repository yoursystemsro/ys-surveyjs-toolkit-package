export { default as CreateTextInputWithSTTWidget, className as TextInputWithSTTWidgetClassName, } from "./CreateWidget";
export { default as RegisterTextInputWithSTTWidget } from "./RegisterWidget";
export type { default as TextInputWithSTTWidget } from "./CreateWidget";
export { default as TextInputWithSTTComponent } from "../../components/TextInputWithSTTWidget/Widget";
