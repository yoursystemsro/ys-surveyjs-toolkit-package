import { WidgetLocalization } from "../../components/TextInputWithSTTWidget";
import type { CreatorJsonMetadata } from "../SurveyJSCreator";
import type { LibraryJsonMetadata } from "../SurveyJSLibrary";
export declare type LocalizationProperty = keyof WidgetLocalization;
export declare const enableSpeechToTextPropertyName = "enableSpeechToText";
export declare const speechToTextTimeLimitPropertyName = "speechToTextTimeLimit";
export declare const localizationProperties: readonly {
    default: string;
    displayName: string;
    name: LocalizationProperty;
}[];
declare const createProperties: (name: string, serializer: CreatorJsonMetadata | LibraryJsonMetadata, voiceSupport: boolean) => void;
export default createProperties;
