import type { CustomWidgetFactory } from "../../utils/types/CustomWidget";
import { TextInputWithSttWidget } from "../../components/TextInputWithSTTWidget";
export declare const className = "yst-widget--text-input-stt";
declare const CreateWidget: CustomWidgetFactory<unknown, TextInputWithSttWidget, {
    voiceSupport: boolean;
}>;
export default CreateWidget;
