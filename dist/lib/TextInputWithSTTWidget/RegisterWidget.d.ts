import type { RegisterCustomWidget } from "../../utils/types/CustomWidget";
/**
 * Helper function to register the custom widget with default values
 * In order for the speech to text part of the component to work, the host
 * component must provide it with onStartRecording property.
 *
 * See an example implementation in ../stories/Playground/TextInputWithSTT.ts
 */
declare const RegisterWidget: RegisterCustomWidget<unknown, unknown, {
    voiceSupport?: boolean;
}>;
export default RegisterWidget;
