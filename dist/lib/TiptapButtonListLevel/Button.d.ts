import type { Editor } from "@tiptap/core";
import { TiptapButton, TiptapButtonProps } from "../../internal/TiptapButton";
export interface ListLevelButtonProps extends TiptapButtonProps {
    variant: "sink" | "lift";
    allowListExit: boolean;
}
export declare enum ListLevelButtonEvents {
    ACTIVATE = "ACTIVATE"
}
export default class ListLevelButton extends TiptapButton {
    static properties: {
        allowListExit: {};
        variant: {};
        label: {};
    };
    allowListExit: ListLevelButtonProps["allowListExit"];
    variant: ListLevelButtonProps["variant"];
    constructor(editor: Editor);
    protected isActive(): boolean;
    protected onClick(e: MouseEvent): void;
}
