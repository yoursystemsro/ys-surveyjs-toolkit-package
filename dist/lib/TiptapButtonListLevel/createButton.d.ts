import type { ButtonFactory } from "../../utils/types/Tiptap";
import type { ListLevelButtonProps } from "./Button";
interface Options {
    props: Partial<ListLevelButtonProps>;
}
declare const createButton: ButtonFactory<Partial<Options>>;
export default createButton;
