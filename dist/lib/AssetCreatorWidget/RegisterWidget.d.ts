import type { RegisterCustomCreatorWidget } from "../../utils/types/CustomWidget";
import type { CreatorWidget, Options } from "./CreateWidget";
declare const RegisterWidget: RegisterCustomCreatorWidget<CreatorWidget, unknown, Options>;
export default RegisterWidget;
