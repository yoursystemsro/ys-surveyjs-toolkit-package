import type { CustomCreatorWidgetFactory } from "../../utils/types/CustomWidget";
import { AssetWidget } from "../AssetWidget";
import { AssetWidgetProps } from "../../components/AssetWidget";
export interface CallbackOptions {
    setAsset: (asset: AssetWidgetProps) => void;
}
export declare type OnSelectCallback = (options: CallbackOptions) => void;
export interface CreatorWidget extends AssetWidget {
    onSelect: (callback: OnSelectCallback) => void;
    removeSelectCallback: (callback: OnSelectCallback) => void;
}
export declare type Options = Partial<{
    selectButtonLabel: string;
    changeButtonLabel: string;
}>;
declare const CreateWidget: CustomCreatorWidgetFactory<CreatorWidget, unknown, Options>;
export default CreateWidget;
