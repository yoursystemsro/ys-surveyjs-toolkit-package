declare enum HtmlContentWidgetTypes {
    BULLET_LIST = "yst-bullet-list",
    COMPOSITE = "yst-composite-text",
    HEADING = "yst-heading-text",
    LINK = "yst-link",
    ORDERED_LIST = "yst-ordered-list",
    SIMPLE = "yst-simple-text"
}
export default HtmlContentWidgetTypes;
