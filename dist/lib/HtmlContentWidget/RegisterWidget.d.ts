import type { RegisterCustomWidget } from "../../utils/types/CustomWidget";
import { Widget } from "./CreateWidget";
import HtmlContentWidgetTypes from "./HtmlContentWidgetTypes";
interface HtmlContentWidgetOptions {
    name: HtmlContentWidgetTypes;
}
declare const RegisterWidget: RegisterCustomWidget<Widget, HTMLDivElement, HtmlContentWidgetOptions>;
export default RegisterWidget;
