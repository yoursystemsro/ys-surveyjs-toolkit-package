import { HtmlContentWidgetTypes } from ".";
import type { CustomWidgetFactory } from "../../utils/types/CustomWidget";
export interface Widget {
    contentPropertyType: string;
    contentPropertyName: string;
}
declare const CreateWidget: CustomWidgetFactory<Widget, HTMLDivElement, {
    customName: HtmlContentWidgetTypes | string;
}>;
export default CreateWidget;
