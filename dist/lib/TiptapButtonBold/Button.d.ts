import type { Editor } from "@tiptap/core";
import { TiptapButton } from "../../internal/TiptapButton";
export default class BoldButton extends TiptapButton {
    constructor(editor: Editor);
    protected isActive(): boolean;
    protected onClick(e: MouseEvent): void;
}
