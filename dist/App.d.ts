import { LitElement, PropertyValues } from "lit";
import { SurveyJSForm } from "./components/SurveyJSForm";
import type { AppConfig } from "./utils/types/AppConfig";
export interface AppProps {
    config?: AppConfig;
}
declare type Properties = Record<keyof typeof App.properties, unknown>;
export default class App extends LitElement {
    static properties: {
        config: {};
    };
    config: AppProps["config"];
    protected form: SurveyJSForm;
    constructor();
    update(props: PropertyValues<Properties>): void;
    protected setFormFields(): void;
    render(): import("lit-html").TemplateResult<1>;
    createRenderRoot(): ShadowRoot;
}
declare const componentName: string;
export { componentName };
