# NPM package for YS Survey JS toolkit

Bitbucket repository for development: [SurveyJS Toolkit](https://bitbucket.org/yoursystemsro/ys-surveyjs-toolkit)

This repository serves only as an NPM private package and is automatically generated
from the build process.

## Installing the package

npm install [--save-dev] bitbucket:yoursystemsro/ys-surveyjs-toolkit-package#1.2.3
